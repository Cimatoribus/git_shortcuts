import sys
from subprocess import call
from .cli_utils import get_bool_from_cli, get_string_from_cli


def git_push():
    """Alias for git push"""

    # Collect extra arguments from the command line
    extra_argv = sys.argv[1:]

    # Make the alias
    return call( ["git", "push"] + extra_argv )


def git_status():
    """Alias for git status"""

    # Collect extra arguments from the command line
    extra_argv = sys.argv[1:]

    # Make the alias
    return call( ["git", "status"] + extra_argv )


def git_branch():
    """Create new local branch"""

    # Collect extra arguments from the command line
    extra_argv = sys.argv[1:]

    # Make the alias
    return call( ["git", "checkout", "-b"] + extra_argv )


def git_add():
    """Alias for git add"""

    # Collect extra arguments from the command line
    extra_argv = sys.argv[1:]

    # Make the alias
    return call( ["git", "add"] + extra_argv )


def git_diff(argv=None):
    """Alias for git diff"""

    # Collect extra arguments from the command line
    extra_argv = sys.argv[1:]

    # Make the alias
    return call( ["git", "diff"] + extra_argv )


def git_undo(argv=None):
    """Undo uncommited changes to a file, the command used is:

    $ git checkout -- <filename>

    """

    # Make the alias
    cli_args = get_string_from_cli(argv,
                                   script_description=git_undo.__doc__,
                                   argument_description="File name")
    return call(["git", "checkout", "--", cli_args.string])


def git_commit_all(argv=None):
    """Alias of git commit, make sure that a non-empty comment is given.
    The following command used is:

    $ git commit [files or -a] -m <commit_message>

    """

    # Start preparing the alias
    git_alias = ["git", "commit"]

    # Collect the extra arguments from the command line, if any
    extra_argv = sys.argv[1:]

    # Add the extra arguments to the alias, these are the files to commit
    if extra_argv:
        git_alias += extra_argv
    # If no file is passed, commit everything
    else:
        print("Committing all modified files.")
        git_alias.append("-a")

    # Collect the commit message
    print("\nEnter a commit code longer than 5 characters:\n")
    commit_message = input()

    # Check if the commit message is good, otherwise exit
    if len(commit_message) < 5:
        print("Invalid commit message, exit.", file=sys.stderr)
        sys.exit(1)

    git_alias += ["-m", commit_message]

    # Create the alias
    return call(git_alias)


def git_clean_all(argv=None):
    """Safely clean the repository from all non-tracked files, by first
    performing a 'dry run'. The command used is:

    $ git clean -x -d -n (for the dry run)

    $ git clean -x -d    (for the actual removal)

    """

    # Read from the command line if we really remove the files
    bool_description = "If given, actually remove the files, otherwise just " \
                       "performs a dry run."
    cli_args = get_bool_from_cli(argv,
                                 script_description=git_clean_all.__doc__,
                                 argument_description=bool_description)

    # Define the arguments for the git alias
    git_alias = ["git", "clean", "-x", "-d"]

    # Add the dry-run option unless actual removal was requested
    if not cli_args.yes:
        git_alias.append("-n")
    # If we remove the files, we must force it
    else:
        git_alias.append("-f")

    return call(git_alias)
