import os
from warnings import warn

EMPTY_STRING = ''


class Project():
    """A class describing a git project"""

    def __init__(self, project_name,
                 base_path='.',
                 author="Name surname", author_email="name.surname@server"):
        import time

        # Generate the metadata of the project
        self.name = project_name
        self.base_path = base_path
        self.full_path = os.path.join(base_path, project_name)
        self.author = author
        self.author_email = author_email

        # Check if base_path exists
        self.have_basedir = os.path.isdir(base_path)

        # List of required directories
        self.required_directories = (
            project_name,
            self.__add_prjname_to("tests"),
            "docs",
            "examples"
        )

        # List of required files
        self.required_files = (
            ".gitignore",
            "README.md",
            "setup.py",
            self.__add_prjname_to("tests/test_initial_files.py"),
            self.__add_prjname_to("__init__.py"),
            "MANIFEST.in"
        )

        # A mapping for substituting the templates
        self.template_mapping = {
            "name": self.name,
            "date": time.strftime("%c"),
            "author": self.author,
            "email": self.author_email
        }

    def __add_prjname_to(self, file_name):
        """Method to add project name as base path to file_name"""
        return os.path.join(self.name, file_name)

    def add_basepath_to(self, file_name):
        """Method to add base path to file_name"""
        return os.path.join(self.full_path, file_name)

    def get_from_template(self, required_file):
        """Method generating a required file from a template"""
        from . import templates
        from string import Template

        # Generate the key for the required file
        required_key = required_file.replace(".", "___dot___") \
                                    .replace("/", "___slash___") \
                                    .replace(self.name, "___project_name___")

        # Load the template if it exists
        try:
            template_file = getattr( templates, required_key )
        except AttributeError:
            warn("Could not find template file for '{}' using key '{}'."
                 .format(required_file, required_key))
            template_file = EMPTY_STRING

        # Substitute the available metadata in the template
        template_file = Template(template_file)
        return template_file.safe_substitute(self.template_mapping)
