import pytest
import os.path as ospath

# Get module base directory name
MODULE_DIR = ospath.abspath('.')
PROJ_NAME = MODULE_DIR.split('/')[-1]

# Constants
IGNORE_FILE = ".gitignore"
README_FILE = "README.md"
MANIFEST_FILE = "MANIFEST.in"
SETUP_FILE = "setup.py"
INIT_FILE = ospath.join(PROJ_NAME, "__init__.py")
TEST_FILE = ospath.join(PROJ_NAME, "tests/test_initial_files.py")

standard_directories = (MODULE_DIR,
                        ospath.join(PROJ_NAME, "tests"),
                        "docs",
                        "examples")

standard_files = (IGNORE_FILE, README_FILE, MANIFEST_FILE,
                  SETUP_FILE,  TEST_FILE,   INIT_FILE)

required_contents = {
    IGNORE_FILE: (
        "build",
        "__pycache__",
        ".pytest_cache",
        "pytest-*",
        ".hypothesis",
        "*.egg-info"
    ),
    README_FILE: (
        PROJ_NAME,
        "Author"
    ),
    SETUP_FILE: (
        'from setuptools import setup, find_packages',
        'name="{}"'.format(PROJ_NAME),
        'depends=["setuptools", "pycodestyle>=2.0"',
        'packages=find_packages(),',
        "version",
        "author",
        "description"
    ),
    TEST_FILE: (
        "def test_directory_structure(directory_name):",
        "def test_existence_standard_files(file_name):",
        "def test_content_standard_files(file_name):",
        "def test_style():",
        "def test_setupdotpy():",
    )
}


@pytest.mark.parametrize("directory_name", standard_directories)
def test_directory_structure(directory_name):
    # Check if required directories exist
    assert ospath.isdir( ospath.join( MODULE_DIR, directory_name ) )


@pytest.mark.parametrize("file_name", standard_files)
def test_existence_standard_files(file_name):
    # Check if required files exist
    assert ospath.isfile( ospath.join( MODULE_DIR, file_name ) )


@pytest.mark.parametrize("file_name", standard_files)
def test_content_standard_files(file_name):
    # Read content of file
    with open(file_name, 'r') as input_file:
        file_content = input_file.read()

    # Check if we expect something from this file
    try:
        required_strings = required_contents[file_name]
    except KeyError:
        return

    # Check for content of generated files
    for required_string in required_strings:
        assert required_string in file_content


def test_style():
    import pycodestyle
    from glob import glob

    # Define the source code checker, ignoring some conventions we don't like
    style_checker = pycodestyle.StyleGuide(
        ignore=["E201", "E202", "E241"]
    )

    # Collect source code files
    source_list = glob('**/*.py', recursive=True)

    # Exclude pytest directories
    source_list = [s for s in source_list if "pytest-" not in s]

    # Do the check
    check_result = style_checker.check_files(source_list)
    assert check_result.total_errors == 0


def test_setupdotpy():
    from subprocess import call

    # try to build the empty package
    call(["python", "setup.py", "build"],
         timeout=120)
