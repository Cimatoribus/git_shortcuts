import pytest


# Test that the script returns an error if no argument is passed
def test_script_noargs():
    from git_shortcuts import git_new_project

    with pytest.raises(SystemExit):
        git_new_project.main()


# Run the tests in the newly created project
def test_new_project(project_dir):
    import subprocess

    return_code = subprocess.call(["pytest", '-s'], cwd=project_dir)
    assert return_code == 0
