import pytest


@pytest.fixture(scope='session')
def project_dir(tmpdir_factory):
    """Prepares a test project"""
    from git_shortcuts import git_new_project
    from shutil import rmtree
    from os.path import join

    # Create a new test project
    git_new_project.main(["test-project",
                          "--basedir", tmpdir_factory.getbasetemp().basename,
                          "--author", "Name Surname",
                          "--email", "myemail@some.server"])

    base_testdir = tmpdir_factory.getbasetemp().basename

    # Return the test project directory
    yield join(base_testdir, 'test-project')

    # Teardown part
    print("Remove test directory")
    rmtree(base_testdir)
