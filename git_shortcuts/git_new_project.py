#!/usr/bin/env python3

description = \
    """Generate the initial directories and files for a new project."""


def cli(argv):
    """Function to parse command line arguments.

    Input:
        None

    Returns:
        namespace object
    """

    import argparse

    # Define the CLI parser
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawDescriptionHelpFormatter )
    parser.add_argument('name', type=str,
                        help='Name of the project')
    parser.add_argument('--basedir', type=str, default=".",
                        help='Directory where the project will be created')
    parser.add_argument('--author', type=str, default="Andrea Cimatoribus",
                        help='Author name')
    parser.add_argument('--email', type=str, default="email@address",
                        help='Contact email')
    parser.add_argument('-v', '--verbose',
                        help='Verbose output', action='store_true')

    # Parse the arguments from the command line
    cli_args = parser.parse_args(argv)

    # Enable to check the arguments being passed
    if cli_args.verbose:
        print(cli_args)

    return cli_args


def main(argv=None):
    # Read the command line, or a list of arguments to be parsed
    args = cli(argv)

    # Make required directories and files
    from git_shortcuts.NewProject import NewProject
    my_new_project = NewProject( args.name,
                                 base_path=args.basedir,
                                 author=args.author, author_email=args.email )
    my_new_project.generate_dirs()
    my_new_project.generate_files()
