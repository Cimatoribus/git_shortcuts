

def get_string_from_cli(argv,
                        script_description='',
                        argument_description='',
                        **kwargs):
    """Function to parse a single command line argument as a string."""
    import argparse

    # Define the CLI parser
    parser = argparse.ArgumentParser(
        description=script_description,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('string', type=str, help=argument_description,
                        **kwargs)

    # Parse the arguments from the command line
    cli_args = parser.parse_args(argv)

    return cli_args


def get_bool_from_cli(argv,
                      script_description='',
                      argument_description='',
                      **kwargs):
    """Function to parse a single boolean option from the command line."""
    import argparse

    # Define the CLI parser
    parser = argparse.ArgumentParser(
        description=script_description,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('--yes', '-y',
                        action='store_true',
                        help=argument_description,
                        **kwargs)

    # Parse the arguments from the command line
    cli_args = parser.parse_args(argv)

    return cli_args
