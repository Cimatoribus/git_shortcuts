"""A file with templates of files to be used in a new git python project"""
import inspect
from git_shortcuts.tests import test_initial_files

___dot___gitignore = """
build
__pycache__
.pytest_cache
pytest-*
.hypothesis
*.egg-info
"""

README___dot___md = """
# $name

One Paragraph of project description goes here

### Prerequisites

Describe the prerequisites

### Installing

A basic installation can be achieved by running
```
python setup.py install
```

## Running the tests

From the root directory of the project, run, e.g.,
```
pytest -s -v --maxfail=1 -pdf
```

## Author

* **$author** - *$email*


Started on: $date

"""

setup___dot___py = """
from setuptools import setup, find_packages

setup(
    name="$name",
    description="",
    packages=find_packages(),
    scripts=[],
    depends=["setuptools", "pycodestyle>=2.0"],
    version='0.1.0',
    author="$author",
    author_email="$email",
    url="",
    license=""
)
"""

___project_name______slash_____init_____dot___py = ""

# To get the test source code, we use introspection
___project_name______slash___tests___slash___test_initial_files___dot___py = \
    inspect.getsource(test_initial_files)

MANIFEST___dot___in = """
include MANIFEST.in
include setup.py
include README.md
recursive-include $name *.py
"""
