import os
from warnings import warn
from .Project import Project


class NewProject(Project):
    """A class for creating a new git project"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Check if the project directory exists already
        if os.path.isdir(self.full_path):
            raise OSError("Directory named '{}' exists."
                          .format(self.full_path))

    def generate_dirs(self):
        """Method to generate standard directories"""

        # Make the directory structure
        for required_dir in self.required_directories:
            os.makedirs( self.add_basepath_to( required_dir ) )

    def generate_files(self):
        """Method to generate the standard files using templates"""

        # Generate all the required files
        for required_file in self.required_files:

            # Open the file to be written
            with open(self.add_basepath_to(required_file), mode="x") \
                    as output_file:

                # Write content to the file based on template
                file_content = self.get_from_template(required_file)
                try:
                    output_file.writelines(file_content)
                except Exception:
                    warn("\nCould not write to file '{}'."
                         .format(required_file))
