# git_shortcuts

A collection of utilities to simplify dealing with GIT repositories

### Prerequisites

Based on the standard library, python 3.6, not other python versions have been tested.

### Installing

A basic installation can be achieved by running
```
pip install .
```

## Running the tests

From the root directory of the project, run, e.g.,
```
pytest -s -v --maxfail=1 -pdb
```

## Author

* **Andrea Cimatoribus** - *andrea.cimatoribus@gmail.com*


Started on: 18 May 2018
