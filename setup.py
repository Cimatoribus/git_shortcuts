from setuptools import setup, find_packages

setup(
    name="git_shortcuts",
    description="Handy tools for helping with GIT repositories",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "git_new_project=git_shortcuts.git_new_project:main",
            "gitp=git_shortcuts.git_aliases:git_push",
            "gitd=git_shortcuts.git_aliases:git_diff",
            "gita=git_shortcuts.git_aliases:git_add",
            "gits=git_shortcuts.git_aliases:git_status",
            "gitb=git_shortcuts.git_aliases:git_branch",
            "gitu=git_shortcuts.git_aliases:git_undo",
            "gitc=git_shortcuts.git_aliases:git_commit_all",
            "gitx=git_shortcuts.git_aliases:git_clean_all"
        ]
    },
    depends=["setuptools", "pycodestyle>=2.0"],
    version='0.1.1',
    author="Andrea Cimatoribus",
    author_email="andrea.cimatoribus@gmail.com",
    url="",
    license=""
)
